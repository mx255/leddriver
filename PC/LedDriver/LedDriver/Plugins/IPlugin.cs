﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LedDriver
{
    public interface IPlugin
    {
        void Run(IHub led);
        void Unload();
    }
}
