﻿namespace LedDriver.Plugins.Default
{
    partial class DefaultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultForm));
            this.l_R = new System.Windows.Forms.Label();
            this.l_G = new System.Windows.Forms.Label();
            this.l_B = new System.Windows.Forms.Label();
            this.s_R = new System.Windows.Forms.HScrollBar();
            this.s_G = new System.Windows.Forms.HScrollBar();
            this.s_B = new System.Windows.Forms.HScrollBar();
            this.b_random = new System.Windows.Forms.Button();
            this.b_palete = new System.Windows.Forms.Button();
            this.b_save = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // l_R
            // 
            this.l_R.AutoSize = true;
            this.l_R.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_R.ForeColor = System.Drawing.Color.Red;
            this.l_R.Location = new System.Drawing.Point(11, 14);
            this.l_R.Name = "l_R";
            this.l_R.Size = new System.Drawing.Size(16, 13);
            this.l_R.TabIndex = 1;
            this.l_R.Text = "R";
            // 
            // l_G
            // 
            this.l_G.AutoSize = true;
            this.l_G.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_G.ForeColor = System.Drawing.Color.Lime;
            this.l_G.Location = new System.Drawing.Point(11, 32);
            this.l_G.Name = "l_G";
            this.l_G.Size = new System.Drawing.Size(16, 13);
            this.l_G.TabIndex = 2;
            this.l_G.Text = "G";
            // 
            // l_B
            // 
            this.l_B.AutoSize = true;
            this.l_B.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_B.ForeColor = System.Drawing.Color.Blue;
            this.l_B.Location = new System.Drawing.Point(12, 51);
            this.l_B.Name = "l_B";
            this.l_B.Size = new System.Drawing.Size(15, 13);
            this.l_B.TabIndex = 3;
            this.l_B.Text = "B";
            // 
            // s_R
            // 
            this.s_R.Location = new System.Drawing.Point(36, 9);
            this.s_R.Maximum = 255;
            this.s_R.Name = "s_R";
            this.s_R.Size = new System.Drawing.Size(329, 19);
            this.s_R.TabIndex = 4;
            this.s_R.Scroll += new System.Windows.Forms.ScrollEventHandler(this.s_R_Scroll);
            // 
            // s_G
            // 
            this.s_G.Location = new System.Drawing.Point(36, 28);
            this.s_G.Maximum = 255;
            this.s_G.Name = "s_G";
            this.s_G.Size = new System.Drawing.Size(329, 19);
            this.s_G.TabIndex = 5;
            this.s_G.Scroll += new System.Windows.Forms.ScrollEventHandler(this.s_G_Scroll);
            // 
            // s_B
            // 
            this.s_B.Location = new System.Drawing.Point(36, 47);
            this.s_B.Maximum = 255;
            this.s_B.Name = "s_B";
            this.s_B.Size = new System.Drawing.Size(329, 19);
            this.s_B.TabIndex = 6;
            this.s_B.Scroll += new System.Windows.Forms.ScrollEventHandler(this.s_B_Scroll);
            // 
            // b_random
            // 
            this.b_random.Location = new System.Drawing.Point(12, 78);
            this.b_random.Name = "b_random";
            this.b_random.Size = new System.Drawing.Size(102, 26);
            this.b_random.TabIndex = 7;
            this.b_random.Text = "Случайно";
            this.b_random.UseVisualStyleBackColor = true;
            this.b_random.Click += new System.EventHandler(this.b_random_Click);
            // 
            // b_palete
            // 
            this.b_palete.Location = new System.Drawing.Point(120, 78);
            this.b_palete.Name = "b_palete";
            this.b_palete.Size = new System.Drawing.Size(102, 26);
            this.b_palete.TabIndex = 9;
            this.b_palete.Text = "Из палитры";
            this.b_palete.UseVisualStyleBackColor = true;
            this.b_palete.Click += new System.EventHandler(this.b_palete_Click);
            // 
            // b_save
            // 
            this.b_save.Location = new System.Drawing.Point(228, 78);
            this.b_save.Name = "b_save";
            this.b_save.Size = new System.Drawing.Size(97, 26);
            this.b_save.TabIndex = 10;
            this.b_save.Text = "Записать";
            this.b_save.UseVisualStyleBackColor = true;
            this.b_save.Click += new System.EventHandler(this.b_save_Click);
            // 
            // DefaultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 111);
            this.Controls.Add(this.b_save);
            this.Controls.Add(this.b_palete);
            this.Controls.Add(this.b_random);
            this.Controls.Add(this.s_B);
            this.Controls.Add(this.s_G);
            this.Controls.Add(this.s_R);
            this.Controls.Add(this.l_B);
            this.Controls.Add(this.l_G);
            this.Controls.Add(this.l_R);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DefaultForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Управление цветом";
            this.Load += new System.EventHandler(this.DefaultForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_R;
        private System.Windows.Forms.Label l_G;
        private System.Windows.Forms.Label l_B;
        private System.Windows.Forms.HScrollBar s_R;
        private System.Windows.Forms.HScrollBar s_G;
        private System.Windows.Forms.HScrollBar s_B;
        private System.Windows.Forms.Button b_random;
        private System.Windows.Forms.Button b_palete;
        private System.Windows.Forms.Button b_save;

    }
}