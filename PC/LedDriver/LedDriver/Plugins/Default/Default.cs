﻿using System.ComponentModel.Composition;

namespace LedDriver.Plugins.Default
{
    [Export(typeof(IPlugin)),
        ExportMetadata("Name", "Стандартный редактор цвета"),
        ExportMetadata("Version", "v0.0.1")]
    public class Default : IPlugin
    {
        private DefaultForm _frm;

        public void Run(IHub led)
        {
            _frm = new DefaultForm(led);
            _frm.Show();
        }

        public void Unload()
        {
            _frm.Close();
        }
    }
}
