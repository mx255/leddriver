﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LedDriver.Plugins.Default
{
    public partial class DefaultForm : Form
    {
        IHub _led;
        Random rnd;

        public DefaultForm(IHub led)
        {
            InitializeComponent();

            _led = led;
        }

        private void DefaultForm_Load(object sender, EventArgs e)
        {
            rnd = new Random(DateTime.Now.Millisecond);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _led.Write(255,0,0);
        }

        private void s_R_Scroll(object sender, ScrollEventArgs e)
        {
            _led.Write((byte)e.NewValue, (byte)s_G.Value, (byte)s_B.Value);
        }

        private void s_G_Scroll(object sender, ScrollEventArgs e)
        {
            _led.Write((byte)s_R.Value, (byte)e.NewValue, (byte)s_B.Value);
        }

        private void s_B_Scroll(object sender, ScrollEventArgs e)
        {
            _led.Write((byte)s_R.Value, (byte)s_G.Value, (byte)e.NewValue);
        }

        private void b_random_Click(object sender, EventArgs e)
        {
            s_R.Value = rnd.Next(0, 255);
            s_G.Value = rnd.Next(0, 255);
            s_B.Value = rnd.Next(0, 255);
            _led.Write((byte)s_R.Value, (byte)s_G.Value, (byte)s_B.Value);
        }

        private void b_palete_Click(object sender, EventArgs e)
        {
            ColorDialog dialog = new ColorDialog();
            
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                s_R.Value = dialog.Color.R;
                s_G.Value = dialog.Color.G;
                s_B.Value = dialog.Color.B;
                _led.Write((byte)s_R.Value, (byte)s_G.Value, (byte)s_B.Value);
            }
        }

        private void b_save_Click(object sender, EventArgs e)
        {
            _led.Save((byte)s_R.Value, (byte)s_G.Value, (byte)s_B.Value);
        }
    }
}
