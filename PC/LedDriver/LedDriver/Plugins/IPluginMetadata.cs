﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace LedDriver
{
    public interface IPluginMetadata
    {
        string Name { get; }
        string Version { get; }
    }
}
