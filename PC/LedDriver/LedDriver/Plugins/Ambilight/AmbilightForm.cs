﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace LedDriver.Plugins.Ambilight
{
    public partial class AmbilightForm : Form
    {
        IHub            _led;
        private Thread  _display_capture_thread;
        private bool    _display_capture_enable;

        public AmbilightForm(IHub led)
        {
            InitializeComponent();

            _led = led;
            _display_capture_enable = false;
            b_disable.Enabled = false;
            b_enable.Enabled = true;
        }

        private void b_enable_Click(object sender, EventArgs e)
        {
            b_enable.Enabled = false;
            b_disable.Enabled = true;
            _display_capture_enable = true;
            _display_capture_thread = new Thread(AverageScreenColorLoop);
            _display_capture_thread.Start();
        }

        private void b_disable_Click(object sender, EventArgs e)
        {
            b_disable.Enabled = false;
            b_enable.Enabled = true;
            _display_capture_enable = false;
        }

        private void AverageScreenColorLoop()
        {
            Size screen_size = Screen.PrimaryScreen.Bounds.Size;
            Bitmap screen_image = new Bitmap(screen_size.Width, screen_size.Height);
            Graphics graphics = Graphics.FromImage(screen_image);
            Bitmap small_image;
            Color pixel;
            int r, g, b, i, j;

            while (_display_capture_enable)
            {
                graphics.CopyFromScreen(Point.Empty, Point.Empty, screen_size);
                small_image = new Bitmap(screen_image, new Size(10, 10));

                r = 0;
                g = 0;
                b = 0;

                for (i = 0; i < 10; i++)
                {
                    for (j = 0; j < 10; j++)
                    {
                        pixel = small_image.GetPixel(i, j);

                        r += (int)pixel.R;
                        g += (int)pixel.G;
                        b += (int)pixel.B;
                    }
                }

                r = r / 100;
                g = g / 100;
                b = b / 100;

                _led.Write((byte)r, (byte)g, (byte)b);

                Thread.Sleep(5);
            }
        }

        private void AmbilightForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _display_capture_enable = false;
        }
    }
}
