﻿using System.ComponentModel.Composition;

namespace LedDriver.Plugins.Ambilight
{
    [Export(typeof(IPlugin)),
        ExportMetadata("Name", "Интерактивная подсветка Ambilight"),
        ExportMetadata("Version", "v0.0.1")]
    public class Default : IPlugin
    {
        private AmbilightForm _frm;

        public void Run(IHub led)
        {
            _frm = new AmbilightForm(led);
            _frm.Show();
        }

        public void Unload()
        {
            _frm.Close();
        }
    }
}
