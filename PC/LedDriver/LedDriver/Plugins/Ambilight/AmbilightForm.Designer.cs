﻿namespace LedDriver.Plugins.Ambilight
{
    partial class AmbilightForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AmbilightForm));
            this.b_enable = new System.Windows.Forms.Button();
            this.b_disable = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // b_enable
            // 
            this.b_enable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b_enable.Location = new System.Drawing.Point(12, 12);
            this.b_enable.Name = "b_enable";
            this.b_enable.Size = new System.Drawing.Size(105, 28);
            this.b_enable.TabIndex = 0;
            this.b_enable.Text = "Включить";
            this.b_enable.UseVisualStyleBackColor = true;
            this.b_enable.Click += new System.EventHandler(this.b_enable_Click);
            // 
            // b_disable
            // 
            this.b_disable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b_disable.Location = new System.Drawing.Point(123, 12);
            this.b_disable.Name = "b_disable";
            this.b_disable.Size = new System.Drawing.Size(105, 28);
            this.b_disable.TabIndex = 1;
            this.b_disable.Text = "Отключить";
            this.b_disable.UseVisualStyleBackColor = true;
            this.b_disable.Click += new System.EventHandler(this.b_disable_Click);
            // 
            // AmbilightForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(235, 48);
            this.Controls.Add(this.b_disable);
            this.Controls.Add(this.b_enable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AmbilightForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Интерактивная подсветка";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AmbilightForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button b_enable;
        private System.Windows.Forms.Button b_disable;
    }
}