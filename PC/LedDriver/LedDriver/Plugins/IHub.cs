﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LedDriver
{
    public interface IHub
    {
        bool IsOpen { get; }

        void Write(byte r, byte g, byte b);

        void Save(byte r, byte g, byte b);

        void Reset();
    }
}
