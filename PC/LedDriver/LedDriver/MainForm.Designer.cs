﻿namespace LedDriver
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.b_connect = new System.Windows.Forms.Button();
            this.l_port = new System.Windows.Forms.Label();
            this.b_console = new System.Windows.Forms.Button();
            this.c_port = new System.Windows.Forms.ComboBox();
            this.l_speed = new System.Windows.Forms.Label();
            this.l_plugins = new System.Windows.Forms.ListBox();
            this.n_speed = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.n_speed)).BeginInit();
            this.SuspendLayout();
            // 
            // b_connect
            // 
            this.b_connect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b_connect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.b_connect.Location = new System.Drawing.Point(298, 7);
            this.b_connect.Name = "b_connect";
            this.b_connect.Size = new System.Drawing.Size(92, 21);
            this.b_connect.TabIndex = 0;
            this.b_connect.Text = "Подключить";
            this.b_connect.UseVisualStyleBackColor = true;
            this.b_connect.Click += new System.EventHandler(this.b_connect_Click);
            // 
            // l_port
            // 
            this.l_port.AutoSize = true;
            this.l_port.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_port.Location = new System.Drawing.Point(12, 10);
            this.l_port.Name = "l_port";
            this.l_port.Size = new System.Drawing.Size(40, 13);
            this.l_port.TabIndex = 2;
            this.l_port.Text = "Порт:";
            // 
            // b_console
            // 
            this.b_console.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b_console.Location = new System.Drawing.Point(396, 7);
            this.b_console.Name = "b_console";
            this.b_console.Size = new System.Drawing.Size(106, 21);
            this.b_console.TabIndex = 3;
            this.b_console.Text = "Консоль порта";
            this.b_console.UseVisualStyleBackColor = true;
            this.b_console.Click += new System.EventHandler(this.b_console_Click);
            // 
            // c_port
            // 
            this.c_port.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.c_port.FormattingEnabled = true;
            this.c_port.Location = new System.Drawing.Point(53, 7);
            this.c_port.Name = "c_port";
            this.c_port.Size = new System.Drawing.Size(84, 21);
            this.c_port.TabIndex = 4;
            // 
            // l_speed
            // 
            this.l_speed.AutoSize = true;
            this.l_speed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.l_speed.Location = new System.Drawing.Point(143, 10);
            this.l_speed.Name = "l_speed";
            this.l_speed.Size = new System.Drawing.Size(67, 13);
            this.l_speed.TabIndex = 5;
            this.l_speed.Text = "Скорость:";
            // 
            // l_plugins
            // 
            this.l_plugins.FormattingEnabled = true;
            this.l_plugins.Location = new System.Drawing.Point(12, 34);
            this.l_plugins.Name = "l_plugins";
            this.l_plugins.Size = new System.Drawing.Size(490, 186);
            this.l_plugins.TabIndex = 6;
            this.l_plugins.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.l_plugins_MouseDoubleClick);
            // 
            // n_speed
            // 
            this.n_speed.Location = new System.Drawing.Point(216, 8);
            this.n_speed.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.n_speed.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.n_speed.Name = "n_speed";
            this.n_speed.Size = new System.Drawing.Size(76, 20);
            this.n_speed.TabIndex = 7;
            this.n_speed.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 228);
            this.Controls.Add(this.n_speed);
            this.Controls.Add(this.l_plugins);
            this.Controls.Add(this.l_speed);
            this.Controls.Add(this.c_port);
            this.Controls.Add(this.b_console);
            this.Controls.Add(this.l_port);
            this.Controls.Add(this.b_connect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LedDriver v0.0.2";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.n_speed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_connect;
        private System.Windows.Forms.Label l_port;
        private System.Windows.Forms.Button b_console;
        private System.Windows.Forms.ComboBox c_port;
        private System.Windows.Forms.Label l_speed;
        private System.Windows.Forms.ListBox l_plugins;
        private System.Windows.Forms.NumericUpDown n_speed;
    }
}

