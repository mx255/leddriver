﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO.Ports;
using System.Runtime.InteropServices;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;

namespace LedDriver
{
    public partial class MainForm : Form
    {
        private LedHub  _led;
        private bool    _show_console;
        private IPlugin _current_plugin;

        [ImportMany]
        public IEnumerable<Lazy<IPlugin, IPluginMetadata>> _plugins;

        public MainForm()
        {
            InitializeComponent();

            ConsoleHelper.Show();
            ConsoleHelper.Hide();

            _led = new LedHub();
            c_port.Items.AddRange(SerialPort.GetPortNames());
            n_speed.Value = 19200;
            _show_console = false;
            _current_plugin = null;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UpdateElements();

            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "plugins");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            AggregateCatalog catalog = new AggregateCatalog(
                new AssemblyCatalog(Assembly.GetExecutingAssembly()),
                new DirectoryCatalog(path, "*.dll"));

            CompositionContainer container = new CompositionContainer(catalog);

            try
            {
                container.ComposeParts(this);
            }
            catch (ChangeRejectedException ex)
            {
                MessageBox.Show("Ошибка при загрузке плагинов! " + ex.Message);
            }

            foreach (Lazy<IPlugin, IPluginMetadata> plugin in _plugins)
            {
                l_plugins.Items.Add(new ListBoxItem(plugin.Metadata.Name, plugin));
            }
        }

        void UpdateElements()
        {
            if (_led.IsOpen)
            {
                c_port.Enabled = false;
                n_speed.Enabled = false;
                b_console.Enabled = true;
                l_plugins.Enabled = true;
            }
            else
            {
                c_port.Enabled = true;
                n_speed.Enabled = true;
                b_console.Enabled = false;
                l_plugins.Enabled = false;
            }
        }

        protected override void WndProc(ref Message m)
        {
            int msg = m.Msg;

            if (msg == 537)
            {
                UpdateElements();

                if (!_led.IsOpen)
                {
                    b_connect.Text = "Подключить";
                    b_connect.ForeColor = Color.Red;
                    _show_console = false;
                    ConsoleHelper.Hide();
                }

                Thread.Sleep(500);
                c_port.Items.Clear();
                c_port.Items.AddRange(SerialPort.GetPortNames());
            }

            base.WndProc(ref m);
        }

        private void b_connect_Click(object sender, EventArgs e)
        {
            if (_led.IsOpen)
            {
                _led.Close();
                b_connect.Text = "Подключить";
                b_connect.ForeColor = Color.Red;
            }
            else
            {
                try
                {
                    _led.Port = (string)c_port.SelectedItem;
                    _led.BaudRate = (int)n_speed.Value;
                    _led.Open();
                    b_connect.Text = "Отключить";
                    b_connect.ForeColor = Color.Green;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            UpdateElements();
        }

        private void b_console_Click(object sender, EventArgs e)
        {
            if (_led.IsOpen)
            {
               if (_show_console)
               {
                   ConsoleHelper.Hide();
               }
               else
               {
                   ConsoleHelper.Show();
               }

               _show_console = !_show_console;
            }
            else
            {
                _show_console = false;
                ConsoleHelper.Hide();
            }
        }

        private void l_plugins_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.l_plugins.IndexFromPoint(e.Location);

            if (index != ListBox.NoMatches)
            {
                ListBoxItem item = (ListBoxItem)l_plugins.Items[index];
                Lazy<IPlugin, IPluginMetadata> plugin = (Lazy<IPlugin, IPluginMetadata>)item.Object;

                if (_current_plugin != null)
                {
                    _current_plugin.Unload();
                }

                _current_plugin = plugin.Value;
                _current_plugin.Run(_led);
            }
        }
    }
}
