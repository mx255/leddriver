﻿using System.IO.Ports;
using System.Threading;
using System;
using System.IO;
using System.Text;

namespace LedDriver
{
    public class LedHub : IHub
    {
        private SerialPort _port;

        public string Port
        {
            get { return _port.PortName; }
            set { _port.PortName = value; }
        }

        public int BaudRate
        {
            get { return _port.BaudRate; }
            set { _port.BaudRate = value; }
        }

        public bool IsOpen
        {
            get { return _port.IsOpen; }
        }
        
        public LedHub(string port, int baudrate)
        {
            _port = new SerialPort();

            _port.PortName = port;
            _port.BaudRate = baudrate;
            _port.DataBits = 8;
            _port.Encoding = Encoding.GetEncoding(28591);
            _port.DataReceived +=_port_DataReceived;
        }

        public LedHub()
        {
            _port = new SerialPort();
            _port.DataBits = 8;
            _port.Encoding = Encoding.GetEncoding(28591);
            _port.DataReceived += _port_DataReceived;
        }

        private void _port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //Console.Write(_port.ReadExisting());
        }

        public void Open()
        {
            _port.Open();
        }

        public void Close()
        {
            _port.Close();
        }

        public void Write(byte r, byte g, byte b)
        {
            if (_port.IsOpen)
            {
                byte[] data = new byte[9];

                data[0] = (byte)'r';
                data[1] = (byte)'g';
                data[2] = (byte)'b';
                data[3] = (byte)'w';
                data[4] = r;
                data[5] = g;
                data[6] = b;
                data[7] = (byte)'4';
                data[8] = (byte)'2';

                try
                {
                    _port.Write(data, 0, 9);
                }
                catch (Exception)
                {

                }

                //Console.WriteLine(BitConverter.ToString(data));
            }
        }

        public void Save(byte r, byte g, byte b)
        {
            if (_port.IsOpen)
            {
                byte[] data = new byte[9];

                data[0] = (byte)'r';
                data[1] = (byte)'g';
                data[2] = (byte)'b';
                data[3] = (byte)'s';
                data[4] = r;
                data[5] = g;
                data[6] = b;
                data[7] = (byte)'4';
                data[8] = (byte)'2';

                try
                {
                    _port.Write(data, 0, 9);
                }
                catch (Exception)
                {

                }

                //Console.WriteLine(BitConverter.ToString(data));
            }
        }

        public void Reset()
        {
            if (_port.IsOpen)
            {
                _port.DtrEnable = true;
                Thread.Sleep(10);
                _port.DtrEnable = false;
            }
        }
    }
}
