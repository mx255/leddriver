﻿using System;

namespace LedDriver
{
    public class ListBoxItem : Object
    {
        public virtual string Name { get; set; }
        public virtual object Object { get; set; }
        
        /// <summary>
        /// Class Constructor
        /// </summary>
        public ListBoxItem()
        {
            this.Name = string.Empty;
            this.Object = null;
        }

        /// <summary>
        /// Overloaded Class Constructor
        /// </summary>
        /// <param name="Name">Object Name</param>
        /// <param name="Object">Object</param>
        public ListBoxItem(string Name, object Object)
        {
            this.Name = Name;
            this.Object = Object;
        }

        /// <summary>
        /// Overloaded Class Constructor
        /// </summary>
        /// <param name="Object">Object</param>
        public ListBoxItem(object Object)
        {
            this.Name = Object.ToString();
            this.Object = Object;
        }

        /// <summary>
        /// Overridden ToString() Method
        /// </summary>
        /// <returns>Object Text</returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}