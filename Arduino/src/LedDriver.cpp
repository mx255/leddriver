
#include <Arduino.h>
#include <EEPROM.h>

#define PWM_RED_PIN 9
#define PWM_GREEN_PIN 10
#define PWM_BLUE_PIN 11

#define SERIAL_BAUDRATE 19200

uint8_t cmd[9];

void cmd_rgbw(void);
void cmd_rgbs(void);
void inline rgb(uint8_t red, uint8_t green, uint8_t blue);

void setup() {
  Serial.begin(SERIAL_BAUDRATE);

  pinMode(PWM_RED_PIN, OUTPUT);
  pinMode(PWM_GREEN_PIN, OUTPUT);
  pinMode(PWM_BLUE_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  TCCR1B = (TCCR1B & B11111000) | B00000001;    // set timer 1 divisor to     1 for PWM frequency of 31372.55 Hz
  TCCR2B = (TCCR2B & B11111000) | B00000001;    // set timer 2 divisor to     1 for PWM frequency of 31372.55 Hz

  digitalWrite(LED_BUILTIN, LOW);

  Serial.print("LedDriver hub v0.0.1\n");

  rgb(255, 0, 0);
  delay(200);
  rgb(0, 255, 0);
  delay(200);
  rgb(0, 0, 255);
  delay(200);

  Serial.print("Read EEPROM...\n");

  uint8_t red = EEPROM.read(0);
  uint8_t green = EEPROM.read(1);
  uint8_t blue = EEPROM.read(2);

  rgb(red, green, blue);

  Serial.print("Ready!\n");
}

void loop() {
  while (Serial.available() > 0) {
    cmd[0] = cmd[1];
    cmd[1] = cmd[2];
    cmd[2] = cmd[3];
    cmd[3] = cmd[4];
    cmd[4] = cmd[5];
    cmd[5] = cmd[6];
    cmd[6] = cmd[7];
    cmd[7] = cmd[8];
    cmd[8] = Serial.read();

    digitalWrite(LED_BUILTIN, LOW);

    if (cmd[0] == 'r' && cmd[1] == 'g' && cmd[2] == 'b' && cmd[7] == '4' && cmd[8] == '2') {
      switch (cmd[3]) {
        case 'w': cmd_rgbw(); break;
        case 's': cmd_rgbs(); break;
      }
    }
  }
}

void cmd_rgbw() {
  digitalWrite(LED_BUILTIN, HIGH);

  rgb(cmd[4], cmd[5], cmd[6]);

  Serial.print("out\n");
}

void cmd_rgbs() {
  digitalWrite(LED_BUILTIN, HIGH);

  EEPROM.update(0, cmd[4]);
  EEPROM.update(1, cmd[5]);
  EEPROM.update(2, cmd[6]);

  Serial.print("save\n");
}

void inline rgb(uint8_t red, uint8_t green, uint8_t blue) {
  analogWrite(PWM_RED_PIN, red);
  analogWrite(PWM_GREEN_PIN, green);
  analogWrite(PWM_BLUE_PIN, blue);
}
